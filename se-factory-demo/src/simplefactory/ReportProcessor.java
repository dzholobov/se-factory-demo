package simplefactory;

public class ReportProcessor {
	
	ReportFactory factory;
	
	public ReportProcessor(ReportFactory factory) {
		this.factory = factory;
	}

	public void prepareReport(String type, String header, String body, String author) {
		Report report = factory.createReport(type, header, body, author);
		
		report.format();
		report.print();
		report.sendTo("archive@organization.com");
	}
	
}
