package simplefactory;

public class MarkdownReport extends Report {

	public MarkdownReport(String header, String body, String author) {
		super(header, body, author);
	}

	@Override
	public void format() {
		header = "# " + header;
		author = "_" + author + "_";
	}

	@Override
	public void print() {
		System.out.println("������ � Markdown");
		super.print();
	}

	@Override
	public void sendTo(String recipient) {
		System.out.println("�������� � ����: ������ Markdown: " + recipient);
	}

	
	
}
