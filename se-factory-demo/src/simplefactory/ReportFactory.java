package simplefactory;

public class ReportFactory {

	public Report createReport(String type, String header, String body, String author) {
		Report report;
		if (type.equals("html"))
			report = new HtmlReport(header, body, author);
		else if (type.equals("md"))
			report = new MarkdownReport(header, body, author);
		else
			report = new Report(header, body, author);
		return report;
	}

	
}
