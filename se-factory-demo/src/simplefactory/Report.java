package simplefactory;

public class Report {

	String header;
	String body;
	String author;
	
	public Report(String header, String body, String author) {
		super();
		this.header = header;
		this.body = body;
		this.author = author;
	}

	public void format() {
		header = "** " + header + " **";
		body = "=====================\n" + body + "\n=====================";
		author = "<<" + author + ">>";
	}
	
	public void print() {
		System.out.println(header);
		System.out.println(body);
		System.out.println(author);
	}
	
	public void sendTo(String recipient) {
		System.out.println("�������� �� ����������� �����: " + recipient);
	}
	
}
