package abstractfactory;

public class HtmlReportFooter implements ReportFooter {

	private String content;
	
	public HtmlReportFooter(String content) {
		this.content = content;
	}

	@Override
	public String formatFooter() {
		return "<center>" + content.toUpperCase() + "</center>";
	}

}
