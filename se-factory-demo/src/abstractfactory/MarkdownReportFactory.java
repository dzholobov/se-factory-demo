package abstractfactory;

import java.util.List;

public class MarkdownReportFactory implements AbstractReportFactory {

	@Override
	public Report createReport(ReportHeader header, List<ReportBodyElement> bodyElements, ReportFooter footer) {
		MarkdownReport report = new MarkdownReport();
		report.setHeader(header);
		for (ReportBodyElement el: bodyElements)
			report.addBodyElement(el);
		report.setFooter(footer);
		return report;
	}

	@Override
	public ReportHeader createHeader(String content) {
		return new MarkdownReportHeader(content);
	}

	@Override
	public ReportBodyElement createBodyElement(String content) {
		return new MarkdownReportBodyElement(content);
	}

	@Override
	public ReportFooter createFooter(String content) {
		return new MarkdownReportFooter(content);
	}

}
