package abstractfactory;

import java.util.ArrayList;
import java.util.List;

public abstract class Report {

	protected ReportHeader header;
	protected List<ReportBodyElement> bodyElements = new ArrayList<>();
	protected ReportFooter footer;
	
	public void setHeader(ReportHeader header) {
		this.header = header;
	}
	
	public void addBodyElement(ReportBodyElement bodyElement) {
		this.bodyElements.add(bodyElement);
	}
	
	public void setFooter(ReportFooter footer) {
		this.footer = footer;
	}
	
	public abstract void print();
	public abstract void save();
	
}
