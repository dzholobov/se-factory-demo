package abstractfactory;

public class MarkdownReportBodyElement implements ReportBodyElement {

	private String content;
	
	public MarkdownReportBodyElement(String content) {
		this.content = content;
	}

	@Override
	public String format() {
		return "  " + content;
	}

}
