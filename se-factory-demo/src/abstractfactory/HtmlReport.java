package abstractfactory;

public class HtmlReport extends Report {

	private HtmlChecker checker = new HtmlChecker();
	
	@Override
	public void print() {
		String result = header.formatHeader();
		for (ReportBodyElement el: bodyElements)
			result += el.format();
		result += footer.formatFooter();
		if (checker.check(result)) {
			System.out.println("������ � ������� HTML");
			System.out.println(result);
		}
	}

	@Override
	public void save() {
		System.out.println("��������� � ������� HTML");
	}


}
