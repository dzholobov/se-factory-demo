package abstractfactory;

public interface ReportHeader {

	public String formatHeader();
	
}
