package abstractfactory;

import java.util.List;

public interface AbstractReportFactory {

	public Report createReport(ReportHeader header, List<ReportBodyElement> bodyElements, ReportFooter footer);
	public ReportHeader createHeader(String content);
	public ReportBodyElement createBodyElement(String content);
	public ReportFooter createFooter(String content);
	
}
