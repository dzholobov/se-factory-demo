package abstractfactory;

public class Runner {

	public static void main(String[] args) {
		String headerText = "����� � ��������";
		String content1Text = "����� ���";
		String content2Text = "������ ���";
		String footerText = "����� ����������: �����������";
		
		HtmlReportFactory htmlFactory = new HtmlReportFactory();
		ReportProcessor rp = new ReportProcessor(htmlFactory);
		rp.prepareReport(headerText, content1Text, content2Text, footerText);
		
		System.out.println();
		
		MarkdownReportFactory mdFactory = new MarkdownReportFactory();
		ReportProcessor rp1 = new ReportProcessor(mdFactory);
		rp1.prepareReport(headerText, content1Text, content2Text, footerText);
	
	}

}
