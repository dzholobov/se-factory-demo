package abstractfactory;

public interface ReportBodyElement {

	public String format();
}
