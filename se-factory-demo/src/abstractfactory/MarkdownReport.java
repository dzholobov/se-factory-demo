package abstractfactory;

public class MarkdownReport extends Report {

	@Override
	public void print() {
		String result = header.formatHeader() + "\n";
		for (ReportBodyElement el: bodyElements)
			result += el.format() + "\n";
		result += footer.formatFooter();
		System.out.println("������ � ������� Markdown");
		System.out.println(result);
	}

	@Override
	public void save() {
		System.out.println("��������� � ������� MD");
	}


}
