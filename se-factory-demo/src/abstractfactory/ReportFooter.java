package abstractfactory;

public interface ReportFooter {

	public String formatFooter();
}
