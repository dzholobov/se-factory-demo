package abstractfactory;

public class MarkdownReportFooter implements ReportFooter {

	private String content;
	
	public MarkdownReportFooter(String content) {
		this.content = content;
	}

	@Override
	public String formatFooter() {
		return "_" + content.toUpperCase() + "_";
	}

}
