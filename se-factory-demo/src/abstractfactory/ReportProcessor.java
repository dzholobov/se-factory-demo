package abstractfactory;

import java.util.Arrays;

public class ReportProcessor {

	private AbstractReportFactory factory;

	public ReportProcessor(AbstractReportFactory factory) {
		this.factory = factory;
	}
	
	public void prepareReport(String headerText, String content1Text, String content2Text, String footerText) {
		ReportHeader header = factory.createHeader(headerText);
		ReportBodyElement content1 = factory.createBodyElement(content1Text);
		ReportBodyElement content2 = factory.createBodyElement(content2Text);
		ReportFooter footer = factory.createFooter(footerText);
		
		Report report = factory.createReport(
				header,
				Arrays.asList(content1, content2),
				footer
				);
		report.print();
		report.save();
	}
	
	
}
