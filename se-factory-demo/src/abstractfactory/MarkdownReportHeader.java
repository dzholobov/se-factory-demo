package abstractfactory;

public class MarkdownReportHeader implements ReportHeader {

	private String content;
	
	public MarkdownReportHeader(String content) {
		this.content = content;
	}

	@Override
	public String formatHeader() {
		String result =  "#" + content;
		return result;
	}

}
