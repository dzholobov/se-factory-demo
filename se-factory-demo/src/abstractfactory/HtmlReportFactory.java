package abstractfactory;

import java.util.List;

public class HtmlReportFactory implements AbstractReportFactory {

	@Override
	public Report createReport(ReportHeader header, List<ReportBodyElement> bodyElements, ReportFooter footer) {
		HtmlReport report = new HtmlReport();
		report.setHeader(header);
		for (ReportBodyElement el: bodyElements)
			report.addBodyElement(el);
		report.setFooter(footer);
		return report;
	}

	@Override
	public ReportHeader createHeader(String content) {
		return new HtmlReportHeader(content, true);
	}

	@Override
	public ReportBodyElement createBodyElement(String content) {
		return new HtmlReportBodyElement(content);
	}

	@Override
	public ReportFooter createFooter(String content) {
		return new HtmlReportFooter(content);
	}

}
