package abstractfactory;

public class HtmlReportHeader implements ReportHeader {

	private String content;
	private boolean strict;
	
	public HtmlReportHeader(String content, boolean strict) {
		this.content = content;
		this.strict = strict;
	}

	@Override
	public String formatHeader() {
		String result =  "<h1>" + content;
		if (strict)
			result += "</h1>";
		return result;
	}

}
