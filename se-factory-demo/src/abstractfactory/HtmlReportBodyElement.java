package abstractfactory;

public class HtmlReportBodyElement implements ReportBodyElement {

	private String content;
	
	public HtmlReportBodyElement(String content) {
		this.content = content;
	}

	@Override
	public String format() {
		return "<p>" + content + "</p>";
	}

}
