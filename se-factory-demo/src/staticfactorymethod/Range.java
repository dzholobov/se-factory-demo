package staticfactorymethod;

public class Range {

	private int min;
	private int max;
	
	/* 
	 * Возможные варианты создания:
	 * min, max - 		[min, max]
	 * min - 			[min, Integer.MAX_VALUE]
	 * max -			[Integer.MIN_VALUE, max]
	 * min, size -		[min, min + size]
	 * center, size -	[center - size, center + size]
	 */

	private Range(int min, int max) {
		this.min = min;
		this.max = max;
	}
	
	// min, max - 		[min, max]
	public static Range between(int min, int max) {
		return new Range(min, max);
	}
	
	// min - 			[min, Integer.MAX_VALUE]
	public static Range minToInfinity(int min) {
		return new Range(min, Integer.MAX_VALUE);
	}
	
	// max -			[Integer.MIN_VALUE, max]
	public static Range minusInfinityToMax(int max) {
		return new Range(Integer.MIN_VALUE, max);
	}
	
	// min, size -		[min, min + size]
	public static Range sizeFromMinimum(int min, int size) {
		return new Range(min, min + size);
	}
	
	// center, size -	[center - size, center + size]
	public static Range sizeFromCenter(int center, int size) {
		return new Range(center - size / 2, center + size / 2);
	}
	
	public int getMin() {
		return min;
	}
	public int getMax() {
		return max;
	}

	public int getSize() {
		return max - min;
	}
	
	public int getCenter() {
		return max - (getSize() / 2);
	}
}
