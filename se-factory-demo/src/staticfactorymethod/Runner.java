package staticfactorymethod;

public class Runner {

	public static void main(String[] args) {
		Range r;
		// [10: 20]
		r = Range.between(10, 20);
		// [10: MAX_INT]
		r = Range.minToInfinity(10);
		// [MIN_INT: 10]
		r = Range.minusInfinityToMax(10);
		// min, size - [10: 20]
		r = Range.sizeFromMinimum(10, 10);
		// center, size - [10: 20]
		r = Range.sizeFromCenter(15, 10);
	}

}
