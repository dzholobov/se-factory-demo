package factorymethod;

public class HtmlReport extends Report {

	public HtmlReport(String header, String body, String author) {
		super(header, body, author);
	}

	@Override
	public void format() {
		header = "<h1>" + header + "</h1>";
		body = "<p style='main text'>\n" + body + "\n</p>";
		author = "<b>" + author + "</b>";
	}

	@Override
	public void print() {
		System.out.println("����� � HTML:");
		super.print();
	}

	@Override
	public void sendTo(String recipient) {
		System.out.println("���-����������: " + recipient);
	}
	
	

}
