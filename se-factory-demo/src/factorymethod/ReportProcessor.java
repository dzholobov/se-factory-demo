package factorymethod;

public abstract class ReportProcessor {
	
	public void prepareReport(String type, String header, String body, String author) {
		Report report = createReport(type, header, body, author);
		
		report.format();
		report.print();
		report.sendTo("archive@organization.com");
	}
	
	public abstract Report createReport(String type, String header, String body, String author);
}
