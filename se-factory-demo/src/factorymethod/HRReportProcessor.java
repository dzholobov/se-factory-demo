package factorymethod;

public class HRReportProcessor extends ReportProcessor {

	@Override
	public Report createReport(String type, String header, String body, String author) {
		Report report;
		if (type.equals("html"))
			report = new HRHtmlReport(header, body, author);
		else if (type.equals("md"))
			report = new HRMarkdownReport(header, body, author);
		else
			report = new Report(header, body, author);
		return report;
	}

}
