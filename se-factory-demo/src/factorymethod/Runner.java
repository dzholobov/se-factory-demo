package factorymethod;

public class Runner {

	public static void main(String[] args) {
		ReportProcessor rp = new AccountingReportProcessor();
		rp.prepareReport("html", "����� �� ��������", "����� ���", "������");
		rp.prepareReport("md", "����� �� ��������", "����� ���", "������");
		System.out.println();
		rp = new HRReportProcessor();
		rp.prepareReport("html", "����� �� ��������", "����� ���", "������");
		rp.prepareReport("md", "����� �� ��������", "����� ���", "������");
		
	}

}
