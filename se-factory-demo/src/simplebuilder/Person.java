package simplebuilder;

public class Person {

	private String firstName; 	// ������������ �������
	private String lastName; 	// ������������ �������
	private int age;			// ������������ �������
	private String address;		// ������������ �������
	private String phone;		// ������������ �������
	
	private Person(PersonBuilder builder) {
		this.firstName = builder.firstName;
		this.lastName = builder.lastName;
		this.age = builder.age;
		this.address = builder.address;
		this.phone = builder.phone;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public static class PersonBuilder {
		private String firstName; 	// ������������ �������
		private String lastName; 	// ������������ �������
		private int age;			// ������������ �������
		private String address;		// ������������ �������
		private String phone;		// ������������ �������

		public PersonBuilder(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
		}
		
		public PersonBuilder age(int age) {
			this.age = age;
			return this;
		}
		
		public PersonBuilder address(String address) {
			this.address = address;
			return this;
		}
		
		public PersonBuilder phone(String phone) {
			this.phone = phone;
			return this;
		}
		
		public Person build() {
			return new Person(this);
		}
	}
	
}
