package simplebuilder;

public class Runner {

	public static void main(String[] args) {
		Person p;
		p = new Person.PersonBuilder("����", "������").
				age(25).
				address("�������� 20").
				phone("(123) 456-78-90").
				build();
		p = new Person.PersonBuilder("����", "������").
				build();
		p = new Person.PersonBuilder("����", "������").
				age(25).
				build();
		p = new Person.PersonBuilder("����", "������").
				address("�������� 20").
				build();
		p = new Person.PersonBuilder("����", "������").
				age(25).
				address("�������� 20").
				build();
		p = new Person.PersonBuilder("����", "������").
				age(25).
				phone("(123) 456-78-90").
				build();
		p = new Person.PersonBuilder("����", "������").
				phone("(123) 456-78-90").
				build();
		p = new Person.PersonBuilder("����", "������").
				address("�������� 20").
				phone("(123) 456-78-90").
				build();

	}

}
