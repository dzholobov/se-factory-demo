package singleton;

public class Connection {

	private static Connection instance;
	
	private Connection() {
		
	}
	
	public static Connection getInstance() {
		if (instance == null)
			instance = new Connection();
		return instance;
	}
	
	public void connect() {
		System.out.println("Connection established");
	}
}
