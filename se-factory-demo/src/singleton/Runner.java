package singleton;

public class Runner {

	public static void main(String[] args) {
		Connection conn = Connection.getInstance();
		conn.connect();
	}
}
